#include <stdio.h>
#include <stdlib.h>

struct Nodo{
    int data;
    struct Nodo *sx;
    struct Nodo *dx;
	};

struct Nodo* makeNodo(int n){
	struct Nodo* NuovoNodo = malloc(sizeof(struct Nodo));
	NuovoNodo->data=n;
	NuovoNodo->sx=NULL;
	NuovoNodo->dx=NULL;
	return NuovoNodo;
	}; 

struct Nodo* inserisci(struct Nodo* tree, int data){
	if(tree==NULL){
		tree = makeNodo(data);
	} else if(data < tree -> data){
		tree -> sx = inserisci(tree -> sx,data);
		}else if(data>tree->data){
		tree->dx = inserisci(tree->dx,data);
		};
	return tree;
	};
	
	void inorder(struct Nodo* tree) {
        if(tree == NULL) return;
        inorder(tree -> sx);
        printf("%d -> ", tree->data);
        inorder(tree->dx);
	}
	
	void preorder(struct Nodo* tree){
	    if(tree == NULL) return;
        printf("%d -> ", tree->data);
        preorder(tree->sx);
        preorder(tree->dx);
	}
	
	void postorder(struct Nodo* tree){
	    if(tree == NULL) return;
        postorder(tree->sx);
        postorder(tree->dx);
        printf("%d -> ", tree->data); 
	}
	
    int cerca(struct Nodo* tree,int num) {
        if(tree == NULL) return NULL;
        if(num == tree->data) return tree->data; 
        if(num < tree->data) return cerca(tree->sx,num);
        if(num > tree->data) return cerca(tree->dx, num);
    }
int main(){
    int x,z;
	struct Nodo *root = NULL;
    root = inserisci(root, 8);
    inserisci(root, 3);
    inserisci(root, 1);
    inserisci(root, 6);
    inserisci(root, 7);
    inserisci(root, 10);
    inserisci(root, 14);
    inserisci(root, 4);
    printf("Elemento da cercare:");scanf("%d", &x);
    z=cerca(root, x);
    if(z==NULL){
        printf("Elemento non trovato \n");
    }else{
        printf("Elemento trovato nel albero \n");
    }
    printf("\n");
    inorder(root);
    printf("\n");
    preorder(root);
    printf("\n");
    postorder(root);
	return 0;
	}
