// ALBERI  alberi - Visite INORDER - PREORDER - POSTORDER

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
struct RTnodo {
   int key;       
   RTnodo *left, *right;
   };
typedef RTnodo* Tnodo;
Tnodo Insert(int, Tnodo, Tnodo);
Tnodo InsertSort(int , Tnodo );
void CreaAlbero(Tnodo& ,int);
void InOrder(Tnodo);
void Preorder(Tnodo);
void Postorder(Tnodo);
void writeTree(Tnodo , int ); 

int main ()  {
   Tnodo A,A1;
   int Num,e;
   RTnodo Nodo;
   cout<<"Inserire 'n' elementi del albero si desidera inserire:";
   cin>>e;
   cout<<endl;
   CreaAlbero(A,e);
   writeTree(A,1);cout<<endl;
      cout<<"\n INORDER "<<endl;
   InOrder( A) ;cout<<endl;
      cout<<"\n PREORDER "<<endl;
   Preorder(A) ;cout<<endl;
      cout<<"\n POSTORDER "<<endl;
   Postorder(A) ;cout<<endl<<endl;
   system("pause");
}

// DEFINIZIONI

Tnodo Insert(int key1, Tnodo ASX, Tnodo ADX) {
//PER INSERIRE UNA FOGLIA SI CHIAMA CON Insert(Numero,NULL,NULL) 
  Tnodo A2;
  A2= new RTnodo;
  A2->key=key1;
  A2->left=ASX;
  A2->right=ADX;
  return A2;
}
Tnodo InsertSort(int key1, Tnodo A)  {
  if (A==NULL)
     return Insert(key1,NULL,NULL);
  else if (A->key>key1)
     return Insert(A->key,InsertSort(key1,A->left),A->right);
     else if (A->key < key1)
        return Insert(A->key,A->left,InsertSort(key1,A->right));
        else
           return A ;
}

void CreaAlbero(Tnodo& A, int dim)  {   
     int Num[dim];
     srand((unsigned)time(NULL));
     A = NULL;
     
     for(int j=0;j<dim;j++){
		 Num[j]=(rand()%1000)+1;
		 }
     
     for(int i = 0; i < dim; i++){ 
      A=InsertSort(Num[i],A);
      cout<<"Ho inserito la chiave ="<<Num[i]<<endl;
	}
}
void writeTree(Tnodo A, int i)  
 {    if (A!=NULL)
     {
        writeTree(A->right,i+1);
        for (int j=1;j<=i;j++)            
            cout<<"  ";            
        cout<<A->key;
        cout<<endl;
        writeTree(A->left,i+1);   }     
}

void InOrder(Tnodo A)  {
  if (A!=NULL)  {
     InOrder(A->left);
     cout<<A->key<<" ";
     InOrder(A->right);
  }  
  else
     cout<<" ";
}

void Preorder(Tnodo A)  {
  if (A!=NULL)  {
  cout<<A->key<<" ";
  Preorder(A->left);
  Preorder(A->right);
  }  
  else
     cout<<" ";
}

void Postorder(Tnodo A)  {
  if (A!=NULL)  {  
  Postorder(A->left);
  Postorder(A->right);
  cout<<A->key<<" ";
  }  
  else
     cout<<" ";
}

